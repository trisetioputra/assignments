package assignments.assignment2;

public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        this.nama=nama;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
        this.bobot=bobot;

        // Note: banyakButirPenilaian digunakan untuk menentukan panjang butirPenilaian saja
        // (tanpa membuat objek-objek ButirPenilaian-nya).
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        this.butirPenilaian[idx]=butir;
    }

    public String getNama() {
        return this.nama;
    }

    public double getRerata() {
        double rata=0;
        double counter=0;
        for(int i=0;i<butirPenilaian.length;i++){
            if(butirPenilaian[i]!=null) {
                rata += butirPenilaian[i].getNilai();
                counter+=1;
            }
        }
        if (counter==0){
            return 0;
        }
        return rata/counter;
    }

    public double getNilai()  {
        return getRerata()*this.bobot/100;
    }

    public String getDetail() {
        System.out.println("~~~ "+getNama()+""+ " ("+this.bobot+"%)"+ " ~~~");
        if (butirPenilaian.length>1) {
            for (int i = 0; i < butirPenilaian.length; i++) {
                if(butirPenilaian[i]!=null){
                    System.out.println(getNama() + " " + (i + 1) + ": " + butirPenilaian[i].toString());
                }
                else {
                    continue;
                }
            }
            System.out.println("Rerata: "+ String.format("%.2f",getRerata()));
        }
        else{
            System.out.println(getNama()+": " +butirPenilaian[0].toString());
        }
        return "Kontribusi nilai akhir: "+ String.format("%.2f",getNilai());
    }

    @Override
    public String toString() {
        return "Rerata " +this.nama+": " + String.format("%.2f",getRerata());
    }

}
