package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        this.nama=nama;
        this.npm=npm;
        this.komponenPenilaian=komponenPenilaian;
        // Note: komponenPenilaian merupakan skema penilaian yang didapat dari GlasDOS.
    }

    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        for(int i=0;i<komponenPenilaian.length;i++){
            if(namaKomponen.equals(komponenPenilaian[i].getNama())){
                return komponenPenilaian[i];
            }
        }
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.
        return null;
    }

    public String getNpm() {
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
                nilai >= 80 ? "A-" :
                        nilai >= 75 ? "B+" :
                                nilai >= 70 ? "B" :
                                        nilai >= 65 ? "B-" :
                                                nilai >= 60 ? "C+" :
                                                        nilai >= 55 ? "C" :
                                                                nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    public String rekap() {
        double nilai_akhir=0;
        String hasil="";
        for (int i=0;i<komponenPenilaian.length;i++){
            hasil+=komponenPenilaian[i].toString()+"\n";
            nilai_akhir+=komponenPenilaian[i].getNilai();
        }
        hasil+="Nilai akhir: "+ String.format("%.2f",nilai_akhir) +"\n";
        hasil+="Huruf: "+ getHuruf(nilai_akhir) + "\n";
        hasil+=getKelulusan(nilai_akhir);
        return hasil;
    }

    public String toString() {
        return this.npm + " - " + this.nama;
    }

    public String getDetail() {
        double nilaiAkhir=0;
        for (KomponenPenilaian penilaian : komponenPenilaian) {
            System.out.println(penilaian.getDetail());
            System.out.println(" ");
            nilaiAkhir += penilaian.getNilai();
        }
        System.out.println("Nilai Akhir: "+ String.format("%.2f",nilaiAkhir));
        System.out.println("Huruf: "+getHuruf(nilaiAkhir));
        return getKelulusan(nilaiAkhir);
    }

    @Override
    public int compareTo(Mahasiswa other) {
        return this.npm.compareTo(other.npm);
    }
}

