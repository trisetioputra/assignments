package assignments.assignment2;


import java.util.ArrayList;
import java.util.List;


public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama) {
        this.kode=kode;
        this.nama=nama;
    }

    public String getKode() {
        return this.kode;
    }
    public void delMahasiswa(Mahasiswa mahasiswa){
        this.mahasiswa.remove(mahasiswa);
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        if(this.mahasiswa.size()==0){
            this.mahasiswa.add(mahasiswa);
        }
        else{
            for(int i=0;i<this.mahasiswa.size();i++){
                if(i==this.mahasiswa.size()-1){
                    if(mahasiswa.compareTo(this.mahasiswa.get(i))>0){
                        this.mahasiswa.add(mahasiswa);
                        break;
                    }
                    else {
                        this.mahasiswa.add(i,mahasiswa);
                        break;
                    }
                }
                else if(mahasiswa.compareTo(this.mahasiswa.get(i))>0){
                    continue;
                }
                else {
                    this.mahasiswa.add(i,mahasiswa);
                    break;
                }
            }
        }
    }

    public Mahasiswa getMahasiswa(String npm) {
        for(int i=0;i<this.mahasiswa.size();i++){
            if(npm.equals(this.mahasiswa.get(i).getNpm())){
                return this.mahasiswa.get(i);
            }
        }
        return null;
    }

    public String rekap() {
        String hasil="";
        for(int i=0;i<this.mahasiswa.size();i++){
            hasil+=this.mahasiswa.get(i)+"\n"+"\n";
            hasil+=this.mahasiswa.get(i).rekap() +"\n"+"\n";
        }
        return hasil;
    }

    public String toString() {
        return this.kode+" - "+this.nama;
    }


}



