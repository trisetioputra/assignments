package assignments.assignment2;

public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    ButirPenilaian(){
        this.nilai=0;
    }

    public ButirPenilaian(double nilai, boolean terlambat) {
        this.nilai=nilai;
        if(nilai<0){
            this.nilai=0;
        }

        this.terlambat=terlambat;
    }


    public double getNilai() {
        if(terlambat==true){
            return (this.nilai-(this.nilai*PENALTI_KETERLAMBATAN/100));
        }
        return nilai;
    }

    @Override
    public String toString() {
        if (terlambat==true){
            return String.format("%.2f",getNilai())+ " (T)";
        }
        return String.format("%.2f",this.nilai);
    }
}

