package assignments.assignment1;

import java.util.Scanner;

public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;

    /**
     *akan menjalankan method utama encode jika dipanggil.
     *
     */
    public static String encode(final String data) {
        final int m = (cari_r(data)); // mencari r
        int[] kosong = tempat(data, m); // menyediakan array dengan panjang input + r
        kosong = atlast(kosong, m); // melakukan algoritma untuk menarih bit redundant yang benar
        String hasil = "";
        for (int i = 0; i < kosong.length; i++) {
            hasil += kosong[i];
        } // karena sebelumnya masih mereturn array, kita jadiin string seperti diminta
        return hasil;
    }

    /**
     *akan menjalankan method utama decode jika dipanggil.
     *
     */
    public static String decode(final String code) {
        int[] a = new int[code.length()]; // menyediakan array kosong sepanjang code
        final int m = cari_r(code); // kita cari ada berapa bit redundant dalam code
        for (int i = 0; i < code.length(); i++) {
            a[i] = Integer.parseInt(code.substring(i, i + 1)); // masukin code ke array kosong
        }
        final int perbaikan = koreksi(a, m); // mencari di posisi bit berapa yang salah
        if (perbaikan != 0) {
            a = yangbener(a, perbaikan); // jika ada bit yang salah, maka akan diperbaiki
        }
        final String hasil = akhirnya(a, m); // menghilangkan bit redunddant

        return hasil;
    }

    /**
     * Main program for Hamming Code.
     *
     * @param args unused
     */
    public static void main(final String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        final Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            final int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                final String data = in.next();
                final String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                final String code = in.next();
                final String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }

    /**
     *akan mengembalikan string yang sudah diseleksi di indeks 2 pangkat.
     *
     */
    public static String akhirnya(final int[] a, final int b) {
        String c = "";
        for (int i = 1; i < a.length + 1; i++) {
            boolean flag = false;
            for (int j = 0; j < b; j++) {
                if (i == Math.pow(2, j)) {
                    flag = true; // jika indeksya == 2 pangkat something, flag nya true
                }
            }
            if (flag == false) {
                c += String.valueOf(a[i - 1]); //c akan ditambah dengan index tersebut
            }
        }
        return c;
    }

    /**
     *akan mengembalikan array yang sudah dikoreksi apabila ada yang salah.
     *
     */
    public static int[] yangbener(final int[] a, final int b) {
        a[b - 1] = 0; // mengganti bit yang salah dengan 0, di min 1 karna di soal indexingnya +1
        return a;
    }

    /**
     *akan int berupa posisi indeks yang salah.
     *
     */
    public static int koreksi(final int[] b, final int z) {
        int a = 1;
        int angk = 0;
        int posisi = 0;
        for (int i = 0; i < z; i++) { // loop sebanyak R
            int count = 0;
            for (int j = a; j < b.length + 1; j += 1) { // loop sepanjang bit yang ingin di decode
                if (count == a) { // akan berhenti ngeloncat sesuai 2 pangkat
                    j += a;
                    count = 0;
                }
                if (j < b.length + 1) { // memastikan tidak keluar indeks
                    angk += b[j - 1]; // buat ngitung ganjil/genap
                    count += 1;
                }

            }
            if (angk % 2 == 1) {
                posisi += a;
            }
            angk = 0;
            a *= 2;
        }
        return posisi;
    }

    /**
     *akan mengembalikan array yang sudah benar untuk di encode.
     *
     */
    public static int[] atlast(final int[] b, final int z) {
        int a = 1;
        int angk = 0;
        for (int i = 0; i < z; i++) { //loop besar sebanyak R
            int count = 0;
            for (int j = a; j < b.length + 1; j += 1) {
                if (count == a) { //untuk loncat
                    j += a;
                    count = 0;
                }
                if (j < b.length + 1) { //menghitung ganjil/genap
                    angk += b[j - 1];
                    count += 1;
                }
            }
            if (angk % 2 == 0) {
                b[a - 1] = 0; //jika genap bit redundant =0
            } else {
                b[a - 1] = 1; //jika ganjil bit redundant =1
            }
            angk = 0;
            a *= 2; //step 2 pangkat
        }
        return b;
    }

    /**
     *akan mengembalikan int berupa R.
     *
     */
    public static int cari_r(final String biner) { // fungsi untuk mencari R

        int hasil = 0;
        int i = 0;
        while (true) {
            if (Math.pow(2, i) >= biner.length() + i + 1) {
                hasil = i; // jika sudah mendapat hasil, loop akan di break
                break;
            }
            i += 1;
        }
        return hasil;
    }

    /**
     *akan mengembalikan array yang menyediakan tempat bagi bagi bit redundant.
     *
     */
    public static int[] tempat(final String biner, final int b) {
        final int x = biner.length() + b; //menyediakan tempat
        final int[] ass = new int[x];
        int pangkat = 0;
        for (int i = 0; i < x; i++) {
            if (Math.pow(2, i) > x) {
                pangkat = i - 1; //menetapkan pangkat maksimum
                break;
            }
        }
        int count = 0;
        for (int i = 0; i < x; i++) { //looping besarnya
            for (int j = 0; j <= pangkat; j++) {
                if (i + 1 == Math.pow(2, j)) { //looping untuk redundant
                    ass[i] = 0;
                    i += 1;
                }

            }
            for (int j = count; j < biner.length(); j++) { //menaruh bit sisa
                ass[i] = Integer.parseInt(biner.substring(j, j + 1));
                count += 1;
                break;
            }
        }
        return ass;
    }


}
