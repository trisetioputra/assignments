package assignments.assignment3;

public class PeganganTangga extends Benda {
  	
    public PeganganTangga(final String nama) {
   
        super(nama);
    }
    public void tambahPersentase(){
        super.setPersentaseMenular(super.getPersentaseMenular()+20);
        
    }
    public String toString(){
        return "PEGANGAN TANGGA "+super.getNama();
    }
    
}