package assignments.assignment3;

public abstract class Manusia extends Carrier{
    private static int jumlahSembuh = 0;
    
    public Manusia(String nama){
        super(nama,"Manusia"); //mengirim parameter nama dan "manusia" sebagai tipe
       
    }
    
    public void tambahSembuh(){
        
        jumlahSembuh+=1;
    }

    public static int getJumlahSembuh() {
        return jumlahSembuh;
      
    }
    public abstract String toString();
    
}