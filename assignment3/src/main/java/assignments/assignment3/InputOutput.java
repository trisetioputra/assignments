package assignments.assignment3;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class InputOutput{
  	
    private BufferedReader br;
    private PrintWriter pw;
    private String inputFile;
    private String outputFile; 
    private World world;

    public InputOutput(String inputType, String inputFile, String outputType, String outputFile){
        this.inputFile=inputFile;
        this.outputFile=outputFile;
        try{ //perlu try catch, karna exception termasuk yang di cek java
            setBufferedReader(inputType);
        }
        catch (FileNotFoundException ex){
            System.out.println("File dak ada");
        }
        try{
            setPrintWriter(outputType);
        }
        catch (FileNotFoundException ex){
            System.out.println("File dak ada");
        }
    }

    public void setBufferedReader(String inputType) throws FileNotFoundException{
        if(inputType.equals("text")){
            br=new BufferedReader(new FileReader("testcases/"+this.inputFile+".txt")); //lokasi file dari gradle
        }
        else{
            br=new BufferedReader(new InputStreamReader(System.in));
        }        
    }
    
    public void setPrintWriter(String outputType) throws FileNotFoundException{
        if(outputType.equals("text")){
            pw=new PrintWriter(new File("testcases/"+this.outputFile+".txt")); 
        }
        else{
            pw=new PrintWriter(System.out);
        }
    }

    
    public void run() throws IOException{
        
        World world=new World();
        String line=br.readLine();
        while(true){
            

            String[] c=line.split(" ");
            String command=c[0].toUpperCase(); //takutnya dia cuma exit dan bukan EXIT seperti TC5

            if(command.equals("EXIT")){
                break;
            }

            if(command.equals("ADD")){
                world.createObject(c[1], c[2]);
            }

            if(command.equals("POSITIFKAN")){
                world.getCarrier(c[1]).ubahStatus("Positif"); //bikin orang positif tiba tiba
            }

            if(command.equals("INTERAKSI")){
                // masuk ke interaksi di carrier
                world.getCarrier(c[1]).interaksi(world.getCarrier(c[2]));
                
            }

            if(command.equals("SEMBUHKAN")){
            
                if(world.getCarrier(c[1]) instanceof PetugasMedis){
                    if(world.getCarrier(c[2]) instanceof Manusia){
                        // cek dia instance petugas medis dan manusia 
                        PetugasMedis babang= (PetugasMedis) world.getCarrier(c[1]); //di casting biar bisa make method di dalam petugas medisnya
                        Manusia sakit=(Manusia) world.getCarrier(c[2]);
                        babang.obati(sakit); 
                       
                    }
                }
            }

            if(command.equals("BERSIHKAN")){

                if(world.getCarrier(c[1]) instanceof CleaningService){
                    if(world.getCarrier(c[2]) instanceof Benda){
                        //cek instance benda dan cleaning service
                        CleaningService cs= (CleaningService) world.getCarrier(c[1]); //di casting biar bisa make method cleaning service
                        Benda bnd= (Benda) world.getCarrier(c[2]);
                        cs.bersihkan(bnd);
                       
                    }
                }
            }

            if(command.equals("TOTAL_KASUS_DARI_OBJEK")){
                Carrier subjek=world.getCarrier(c[1]);
                int result= world.getCarrier(c[1]).getTotalKasusDisebabkan();
                pw.write(subjek.toString()+" telah menyebarkan virus COVID ke "+result+" objek"+"\n"); 
            }

            if(command.equals("AKTIF_KASUS_DARI_OBJEK")){
                Carrier subjek=world.getCarrier(c[1]);
                int result= world.getCarrier(c[1]).getAktifKasusDisebabkan();
                pw.write(subjek.toString()+" telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak "+result+ " objek"+"\n");
            }

            if(command.equals("TOTAL_SEMBUH_MANUSIA")){
                int total=0;
                for(int i=0; i< world.getListCarrier().size();i++){
                    if(world.getListCarrier().get(i) instanceof PetugasMedis){
                        PetugasMedis ptg= (PetugasMedis) world.getListCarrier().get(i);
                        total+= ptg.getJumlahDisembuhkan();
                        // meloop semua instance tenaga medis, dan mengambil jumlah yang disembuhkan oleh tiap petugas
                    }
                }
                pw.write("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak "+ total+" kasus"+"\n");
            }

            if(command.equals("TOTAL_SEMBUH_PETUGAS_MEDIS")){
                Carrier subjek= world.getCarrier(c[1]);
                if(world.getCarrier(c[1]) instanceof PetugasMedis){
                    PetugasMedis pt=(PetugasMedis) world.getCarrier(c[1]);
                    //menggunakan method getJumlahDisembuhkan, maka harus pake instance of dan casting lagi
                    pw.write(subjek.toString()+" menyembuhkan "+ pt.getJumlahDisembuhkan()+ " manusia"+"\n");
                }
            }

            if(command.equals("TOTAL_BERSIH_CLEANING_SERVICE")){
                Carrier subjek = world.getCarrier(c[1]);
                if(world.getCarrier(c[1]) instanceof CleaningService){
                    CleaningService cl=(CleaningService) world.getCarrier(c[1]);
                    //menggunakan method getJumlahDibersihkan, maka harus pake instance of dan casting lagi

                    pw.write(subjek.toString()+" membersihkan "+ cl.getJumlahDibersihkan()+ " benda"+"\n");
                }
            }
            
            if(command.equals("RANTAI")){
                
                String hasil="";
                Carrier subjek=world.getCarrier(c[1]);
                try{ //menggunakan try catch terhadap belumtertularexception
                    if(subjek.getStatusCovid().equals("Positif")){
                        for(int i=0;i<subjek.getRantaiPenular().size();i++){ //meloop rantainya
                            hasil+=subjek.getRantaiPenular().get(i)+" -> ";
                        }
                        hasil+=subjek; //menambahkan dirinya sendiri di akhir
                        pw.write("Rantai penyebaran "+ world.getCarrier(c[1]).toString()+ ": "+hasil +"\n");
                    }
                    else{ //melempar belumtertular
                        throw new BelumTertularException(subjek.toString()+" berstatus negatif");
                    }
                
                }
                catch (BelumTertularException ex){ //menangkap belum tertular exception
                    pw.write(ex+"\n"); //print messagenya
                }
                
            }
            
           

            line=br.readLine(); //membaca baris baru
        }
        
        pw.close();   
       
    }
    
   

}
    
       

