package assignments.assignment3;

public class PetugasMedis extends Manusia{
  		
    private int jumlahDisembuhkan;

    public PetugasMedis(String nama){
        
        super(nama);
    }

    public void obati(Manusia manusia) {
       
        manusia.ubahStatus("Negatif"); //mengubah status covid menjadi negatif
        this.jumlahDisembuhkan+=1;
        super.tambahSembuh();
       
        // menambah jumlah orang sembuh
    }

    public int getJumlahDisembuhkan(){
        // Kembalikan nilai dari atribut jumlahDisembuhkan
        return this.jumlahDisembuhkan;
    }
    public String toString(){
        return "PETUGAS MEDIS "+super.getNama();
    }
}