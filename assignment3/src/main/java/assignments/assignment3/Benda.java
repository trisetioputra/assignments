package assignments.assignment3;


public abstract class Benda extends Carrier{
  
    protected int persentaseMenular;

    public Benda(String name){
        super(name,"Benda");
        // TODO: Buat constructor untuk Benda.
        // Hint: Akses constructor superclass-nya
    }

    public abstract void tambahPersentase();

    public int getPersentaseMenular(){
        // TODO : Kembalikan nilai dari atribut persentaseMenular
        return this.persentaseMenular;
    }
    
    public void setPersentaseMenular(int persentase) {
        this.persentaseMenular=persentase;
        if(this.persentaseMenular>=100){
            this.ubahStatus("Positif");
        }
        // TODO : Gunakan sebagai setter untuk atribut persentase menular
    }
    public abstract String toString();
}