package assignments.assignment3;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.nio.file.FileAlreadyExistsException;
import java.io.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class SimulatorGUI extends Application {

    private PrintWriter pw;
    private BufferedReader br;
    String result="";


    @Override
    public void start(Stage stage) throws Exception{


        // jangan lupa mengganti path file InputOutput jika ingin menggunakan GUI

        
        try{
            pw=new PrintWriter(new File("testcases/this"+".txt")); //printwriter buat nanti baca file temporary
        }
        catch (FileNotFoundException ex){
            System.out.println("");
        }
        
        GridPane gp = new GridPane();
        gp.setAlignment(Pos.CENTER);
        gp.setVgap(10);
        gp.setHgap(10);
        gp.setPadding(new Insets(25, 25, 25, 25));
        VBox pn= new VBox();
        FileInputStream fis = new FileInputStream("Foto.jpg"); 
        Image img = new Image(fis);  
        ImageView iv = new ImageView(img); 
        HBox greetings = new HBox();
        iv.setFitHeight(200);
        iv.setFitWidth(200);
        pn.setAlignment(Pos.CENTER);
        pn.setSpacing(10);
      
        pn.getChildren().add(iv) ;
        Text hello = new Text("Selamat datang di program simulasi COVID-19!");
        hello.setFont(Font.font("roboto",25));
        pn.getChildren().add(hello);
        gp.add(pn,0,0,2,1); 

        gp.add(new Label("Method Input"),0,3);
        String[] Method = {"text","GUI"};
        ChoiceBox methods = new ChoiceBox(FXCollections.observableArrayList(Method));
        gp.add(methods,1,3);

        gp.add(new Label("Nama File"),0,4);
        TextField inputfile = new TextField();
        gp.add(inputfile,1,4);

        gp.add(new Label("Method Output"),0,5);
        String[] Method2 = {"text","GUI"};
        ChoiceBox methods2 = new ChoiceBox(FXCollections.observableArrayList(Method2));
        gp.add(methods2,1,5);

        gp.add(new Label("Nama File Output"),0,6);
        TextField outputfile = new TextField();
        gp.add(outputfile,1,6);

        gp.add(new Text("(Kosongkan seluruh nama file jika input menggunakan GUI)"),0,7);
        gp.add(new Text("Program ini belum bisa menerima input disini(GUI) dan dituliskan ke file"),0,8);
        gp.add(new Text("Program ini bisa dari file ke file, file ke GUI, dan dari GUI ke GUI"),0,9);


        Button btn = new Button("Next"); 
        gp.add(btn,0,10);
        Scene first=new Scene(gp,700,600);

        //gp adalah tampilan awal yang berisi pilihan user ingin menggunakan metode input 
        //metode output dan nama filenya

        GridPane gp2 = new GridPane();
        gp2.setAlignment(Pos.CENTER);
        gp2.setVgap(10);
        gp2.setHgap(10);
        gp2.setPadding(new Insets(25, 25, 25, 25));
        gp2.add(new Text("Terimakasih, jaga kesehatan gan, output sudah dibuat"),0,7);
        Button btn2 = new Button("Exit"); 
        gp2.add(btn2,0,8);
       
       
        Scene sec=new Scene(gp2,600,600);

        //gp2 dan scene sec merupakan scene ketika user ingin input dan outputnya pakai teks

        GridPane gp3 = new GridPane();
        gp3.setAlignment(Pos.CENTER);
        gp3.setVgap(10);
        gp3.setHgap(10);
        gp3.setPadding(new Insets(25, 25, 25, 25));
        gp3.add(new Text("Silakan isi gan"),0,2);
        gp3.add(new Label("Perintah: "),0,4);
        TextField inputcommand = new TextField();
        gp3.add(inputcommand,1,4);
        Button btn3 = new Button("Next"); 
        gp3.add(btn3,0,8);
        Button btn4 = new Button("Submit"); 
        gp3.add(btn4,2,8);
        gp3.add(new Text("Kalau udah selesai pencet Submit"),0,9);

       
       
        Scene thir=new Scene(gp3,600,600);
        //gp3 dan thir adalah ketika user ingin input dari sini dan output dari sini juga

        GridPane gp4 = new GridPane();
        gp4.setVgap(10);
        gp4.setHgap(10);
        gp4.add(new Text("Berikut hasil Input"),1,1);
        Button btn5 = new Button("Exit"); 
        gp4.add(btn5,1,10);
        Scene forth=new Scene(gp4,800,500);
        
        //gp4 dan forth apabila user input dari teks atau dari GUI namun output ke sini (GUI)

        
        btn.setOnAction(event -> {
            String method1=methods.getValue().toString(); //membaca pilihan methode user menjadi String
            String method2=methods2.getValue().toString();

            String inputFile=inputfile.getText();
            String outputFile=outputfile.getText();

            if(method1.equals("text") && method2.equals("text")){ //jika input text dan output text
                InputOutput io= new InputOutput(method1, inputFile, "text", outputFile);
                //menggunakan class InputOutput untuk mendeclare instance nya
                try{
                    io.run(); //me run program seperti biasa
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                stage.setScene(sec); //menunjugan gp2 dan sec
                stage.show();
            }
            else if(method1.equals("text") && method2.equals("GUI")){ //jika input text dan output GUI
                InputOutput io= new InputOutput(method1, inputFile, "text", "that");
                //tetap membaca biasa text inputan, namun tar di printwriterin ke file temporary / file tampungan
                try{
                    io.run();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                try{
                    //kita baca dari file tampungan
                    br=new BufferedReader(new FileReader("testcases/that"+".txt")); 
                    
                    try{
                        String line = br.readLine(); 
                        while(line!=null){
                            result+=line+"\n";
                            line=br.readLine();
                        }
                        //loop file tampungan dan memasukkannya ke result, yakni that.txt
                        Text hasil= new Text(result); //menjadikan result sebagai text yang bisa dibaca GUI
                        gp4.add(hasil, 1, 3); //memasukkan hasil ke gp4
    
    
                    }
                    catch (IOException ex){
    
                    }                    
                }
                catch (FileNotFoundException ex){
                    System.out.println("");
                }     
                stage.setScene(forth); //menampilkan gp4 dan scene Forth
                stage.show();

            }
            else{ //jika GUI dan GUI
                stage.setScene(thir);
                stage.show();
            }
            

        });

        btn3.setOnAction(event -> { //untuk menulis ke file inputan temporary
            stage.setScene(thir);
            stage.show();
            pw.write(inputcommand.getText()+"\n"); //menggunakan instance pw yang didefinisikan di class ini mengambil input user
            inputcommand.clear();

        });

        btn2.setOnAction(event -> {
            System.exit(0);
            
        
        });
        btn5.setOnAction(event -> {
            System.exit(0);
            
        
        });
        btn4.setOnAction(event -> { //ini tombol submit
            pw.write("EXIT"); //biar gak error, harus di EXIT secara otomatis apabila user nge submit
            pw.close();
            // disini kita udah ada file isi inputan dari user, sekarang kita kelola
            InputOutput io =new InputOutput("text", "this","text", "that"); //that file dan this file, file temporary untuk hasil
            try{
                io.run(); 
            }
            catch (Exception e){
                e.printStackTrace();
            }
            //nah disini kita udah ada file output sementara yang isinya hasil dari IO.run()
            try{
                br=new BufferedReader(new FileReader("testcases/that"+".txt")); 
                
                try{ //kita baca file hasilnya
                    String line = br.readLine();
                    while(line!=null){
                        result+=line+"\n"; //dimasukkan ke result
                        line=br.readLine();
                    }
                    Text hasil= new Text(result);
                    gp4.add(hasil, 1, 3); // ditambahkan ke gp4


                }
                catch (IOException ex){

                }
                
            }
            catch (FileNotFoundException ex){
                System.out.println("");
            }     
            stage.setScene(forth);
            stage.show();
        
        });
        
        
        stage.setScene(first);
        stage.show();

    }
    public static void main(String[] args) {
        launch();
    }
    

}
