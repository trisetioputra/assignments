package assignments.assignment3;

public class Pintu extends Benda{
    
    public Pintu(String name){
        super(name);
       
    }
    public void tambahPersentase(){
        super.setPersentaseMenular(super.getPersentaseMenular()+30);
    }
    public String toString(){
        return "PINTU "+super.getNama();
    }
}