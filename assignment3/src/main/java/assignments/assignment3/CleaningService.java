package assignments.assignment3;

public class CleaningService extends Manusia{
  		
    private int jumlahDibersihkan;

    public CleaningService(String nama){

        super(nama);
    }

    public void bersihkan(Benda benda){
        if(benda.getPersentaseMenular()<100 && benda.getPersentaseMenular()>=0){
            benda.setPersentaseMenular(0); //di reset jadi 0
        }
        else if(benda.getPersentaseMenular()>=100){
            benda.setPersentaseMenular(0); //di reset jadi 0
            benda.ubahStatus("Negatif");  //mengubah status covid suatu benda apabila benda sudah positif

        }
        this.jumlahDibersihkan+=1;
      
    }

    public int getJumlahDibersihkan(){
        // Kembalikan nilai dari atribut jumlahDibersihkan
        return this.jumlahDibersihkan;
    }
    public String toString(){
        return "CLEANING SERVICE "+super.getNama();
    }

}