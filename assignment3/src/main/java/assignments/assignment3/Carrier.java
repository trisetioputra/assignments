package assignments.assignment3;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;

public abstract class Carrier{

    private String nama;
    private String tipe;
    private Status statusCovid;
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular;
    private List<Carrier> reverseRantai;

    public Carrier(String nama,String tipe){
        this.nama=nama;
        this.tipe=tipe;
        this.statusCovid=new Negatif();
        rantaiPenular=new ArrayList<>();
        reverseRantai=new ArrayList<>();

    }

    public String getNama(){
        // Kembalikan nilai dari atribut nama
        return this.nama;
    }

    public String getTipe(){
        // Kembalikan nilai dari atribut tipe
        return this.tipe;
    }

    public String getStatusCovid(){
        // Kembalikan nilai dari atribut statusCovid
        return this.statusCovid.getStatus();
    }

    public int getAktifKasusDisebabkan(){
        ArrayList<Carrier> contain=new ArrayList<>(); //membuat arraylist untuk menampung objek objek
        int a=0;
        for(int i=0; i<this.reverseRantai.size();i++){
            if(reverseRantai.get(i).getStatusCovid().equals("Positif") && !(contain.contains(reverseRantai.get(i)))){
                a+=1;
                contain.add(reverseRantai.get(i)); 
                // intinya dia bakal di filter , biar gak ada pengulangan orang di list contain dan harus positif
            }
            else{
                // kalo udah ada di list contain, sekip
                continue;
            }
        }
        
        return a;
    }

    public int getTotalKasusDisebabkan(){
        ArrayList<Carrier> contain=new ArrayList<>();
        int a=0;
        for(int i=0; i<this.reverseRantai.size();i++){
            if(!(contain.contains(reverseRantai.get(i)))){
                a+=1;
                contain.add(reverseRantai.get(i));
                // intinya dia bakal di filter , biar gak ada pengulangan orang di list contain 

            }
            else{
                // kalo udah ada di list contain, sekip

                continue;
            }
        }
       
        return a;
        
        
    }

    public List<Carrier> getRantaiPenular(){
        
        return this.rantaiPenular;
    }
    public List<Carrier> getReverseRantai(){
        return this.reverseRantai;
    }
    
    public void ubahStatus(String status){
        // mengubah status dengan membuat membuat instance yang sesuai
        if(status.equals("Positif")){
            this.statusCovid=new Positif();
        }
        else{
            this.statusCovid=new Negatif();
        
        }
       
    }

    public void interaksi(Carrier lain) {
        if(this.getStatusCovid().equals("Positif")&& lain.getStatusCovid().equals("Positif")){
            //kalo positif dan positif gak ngapa ngapain meskipun benda juga gapapa karna kalo benda dah positif, presentasenya dah gangaruh lagi
        }
        
        else if(this.getStatusCovid().equals("Positif")){
            if(lain.getTipe().equals("Benda")){
                if(this.getTipe().equals("Benda")){

                }
                else{
                    Benda a= (Benda) lain;
                    a.tambahPersentase();
                    if(a.getStatusCovid().equals("Positif")){
                        if(lain.rantaiPenular.size()!=0){ //kalo dia nularin seseorang yang dah punya rantai
                            lain.rantaiPenular.clear();  //rantai satunya di ilangin
                            lain.reverseRantai.remove(lain);
                            
                        }   
                        if(this.rantaiPenular.size()==0){ 
                            lain.rantaiPenular.add(this); //misal rantai c isinya [a,b]
                            this.reverseRantai.add(lain); //reversenya a =[b,c], b=[c]
                        }
                        else{ //else punya if yang diatas ini
                            for(int i=0;i<this.rantaiPenular.size();i++){ //looping rantai babang yang nularin, karna orang yang nularin si this
                                lain.rantaiPenular.add(this.getRantaiPenular().get(i)); 
                                this.getRantaiPenular().get(i).reverseRantai.add(lain);
                            }
                            lain.rantaiPenular.add(this); //nge add si babang juga
                            this.reverseRantai.add(lain);
                        }
                    }
                }
            }
            else{
                this.statusCovid.tularkan(this, lain); //tularkeun
                if(lain.rantaiPenular.size()!=0){ //kalo dia nularin seseorang yang dah punya rantai
                    lain.rantaiPenular.clear(); //rantai satunya di ilangin
                    lain.reverseRantai.remove(lain);
                    
                }   
                if(this.rantaiPenular.size()==0){
                    lain.rantaiPenular.add(this); //misal rantai c isinya [a,b]
                    this.reverseRantai.add(lain); //reversenya a =[b,c], b=[c]
                }
                else{
                    for(int i=0;i<this.rantaiPenular.size();i++){ //looping rantai babang yang nularin, karna orang yang nularin si this
                        lain.rantaiPenular.add(this.getRantaiPenular().get(i));
                        this.getRantaiPenular().get(i).reverseRantai.add(lain);
                    }
                    lain.rantaiPenular.add(this);//nge add si babang juga
                    this.reverseRantai.add(lain);
                }
            }
        }
        else if(lain.getStatusCovid().equals("Positif")){ //memastikan yang postif jadi this
            lain.interaksi(this);
        }
        else{ //jika negatif dan negatif
            
        }

        
        // TODO : Objek ini berinteraksi dengan objek lain
    }

    public abstract String toString();

}
