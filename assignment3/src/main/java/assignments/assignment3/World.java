package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World{

    public List<Carrier> listCarrier;

    public World(){

        listCarrier=new ArrayList<>();
    
    }

    public void createObject(String tipe, String nama){
        // Implementasikan apabila ingin membuat object sesuai dengan parameter yang diberikan
        switch(tipe){
            case "JURNALIS":
                Jurnalis one= new Jurnalis(nama);
                listCarrier.add(one);
                break;

            case "OJOL":
                Ojol two= new Ojol(nama);
                listCarrier.add(two);
                break;

            case "PINTU":
                Pintu three= new Pintu(nama);
                listCarrier.add(three);
                break;

            case "TOMBOL_LIFT":
                TombolLift four= new TombolLift(nama);
                listCarrier.add(four);
                break;

            case "PEKERJA_JASA":
                PekerjaJasa five= new PekerjaJasa(nama);
                listCarrier.add(five);

            case "CLEANING_SERVICE":
                CleaningService six=new CleaningService(nama);
                listCarrier.add(six);
                break;

            case "PETUGAS_MEDIS":
                PetugasMedis seven=new PetugasMedis(nama);
                //listPetugas.add(seven);
                listCarrier.add(seven);
                break;

            case "PEGANGAN_TANGGA":
                PeganganTangga eight=new PeganganTangga(nama);
                listCarrier.add(eight);
                break;

            case "ANGKUTAN_UMUM":
                AngkutanUmum nine=new AngkutanUmum(nama);
                listCarrier.add(nine);
                break;

        }
       
    }

    public Carrier getCarrier(String nama){
        // Implementasikan apabila ingin mengambil object carrier dengan nama sesuai dengan parameter
        for(int i=0;i<listCarrier.size();i++){
            if(nama.equals(listCarrier.get(i).getNama())){
                return listCarrier.get(i);
            }
        }
        return null;
    }
    public List<Carrier> getListCarrier(){
        return this.listCarrier;
    }
}
