package assignments.assignment3;

public class AngkutanUmum extends Benda{
    // TODO: Implementasikan abstract method yang terdapat pada class Benda
      
    public AngkutanUmum(String nama){
        super(nama);
        // TODO: Buat constructor untuk AngkutanUmum.
        // Hint: Akses constructor superclass-nya
    }
    public void tambahPersentase(){
        super.setPersentaseMenular(super.getPersentaseMenular()+35);
    }
    public String toString(){
        return "ANGKUTAN UMUM "+ super.getNama();
    }
}